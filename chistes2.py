#!/usr/bin/python3
# -*- coding: utf-8 -*-

from xml.sax import make_parser
from xml.sax.handler import ContentHandler


class ChistesHandler(ContentHandler):
    """
    Clase para manejar chistes malos
    """

    def __init__(self):
        """
        Constructor. Inicializamos las variables
        """
        super().__init__()
        self.calificacion = ""
        self.pregunta = ""
        self.inPregunta = False
        self.respuesta = ""
        self.inRespuesta = False
        self.count = 0
        self.orden = ""  # creamos esta variable para ordenar primero la pregunta y

    # despues la respuesta y que no nos aparezca desordenado

    def startElement(self, name, attrs):
        """
        Método que se llama cuando se abre una etiqueta
        """
        if name == 'chiste':  # De esta manera tomamos los valores de los atributos
            self.orden = name  # rellenamos la variable con name (chiste)
            self.calificacion = attrs.get('calificacion', "")
            self.count = self.count + 1
            print(f"Chiste número: {self.count} ({self.calificacion})")

        elif name == 'pregunta':
            self.orden = name  # rellenamos la variable con name (pregunta)
            self.pregunta = ""
            self.inPregunta = True

        elif name == 'respuesta':
            self.orden = name  # rellenamos la variable con name (respuesta)
            self.respuesta = ""
            self.inRespuesta = True

    # La variable orden = name la usaremos luego en un if para ordenar

    def endElement(self, name):
        """
        Método que se llama al cerrar una etiqueta
        """
        if name == 'pregunta':
            # print(''+self.pregunta)
            self.inPregunta = False
        elif name == 'respuesta':
            # print(''+self.respuesta)
            self.inRespuesta = False
        elif name == 'chiste':  # Cada vez que pase a un chiste se vacían las "etiquetas"
            # de pregunta y respuesta. Sino nos daria problemas en el if de despues
            self.pregunta = ""
            self.respuesta = ""

    def characters(self, char):
        """
        Método para tomar contenido de la etiqueta
        """
        if self.inPregunta:
            self.pregunta = self.pregunta + char
        elif self.inRespuesta:
            self.respuesta += char

        # Ahora creamos el if que nos servira para ordenar las preguntas y respuestas
        if self.pregunta != "":
            if self.respuesta != "":
                if char == "\n":
                    if self.orden == "respuesta":
                        self.orden = "rellenado"
                        print(f"Pregunta: {self.pregunta}")
                        print(f"Respuesta: {self.respuesta}\n")

                    if self.orden == "pregunta":
                        self.orden = "rellenado"
                        print(f"Pregunta: {self.pregunta}")
                        print(f"Respuesta: {self.respuesta}\n")


def main():
    """Programa principal"""
    parser = make_parser()  # analizador
    cHandler = ChistesHandler()  # objeto cHandler de la clase ChistesHandler
    parser.setContentHandler(cHandler)
    parser.parse(open("chistes.xml"))  # empieza a analizar el fichero chistes.xml


if __name__ == "__main__":
    main()
