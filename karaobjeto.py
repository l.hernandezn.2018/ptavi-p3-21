#!/usr/bin/python3
# -*- coding: utf-8 -*-

from karaoke import make_parser, SMILHandler, parse_arg, to_json
from urllib.request import urlretrieve
import sys


class Karaoke:
    def __init__(self, nombre):
        parser = make_parser()
        cHandler = SMILHandler()
        parser.setContentHandler(cHandler)
        parser.parse(open(nombre))
        self.etiquetas = cHandler.get_tags()

    def __str__(self):
        return to_string(self.etiquetas)  # No consigo ver porque to_string me da error

    def to_json(self):
        return to_json(self.etiquetas)

    def fichero(self):
        fichero(self.etiquetas)


def fichero(etiquetas):
    n = 0  # Iniciamos la varibale en 0
    for etiqueta in etiquetas:
        for att, url in etiqueta['attrs']:
            if att == 'src':
                nombre_arch = url.split('/')[-1]
                ext = nombre_arch.split('.')[-1]
                urlretrieve(url, f'fichero-{n}.{ext}')
                n += 1


def main():
    json, nombre = parse_arg()
    karaobjeto = Karaoke(nombre)
    if json:
        print(karaobjeto.to_json())
    else:
        print(str(karaobjeto), end='')
    karaobjeto.fichero()


if __name__ == '__main__':
    main()
