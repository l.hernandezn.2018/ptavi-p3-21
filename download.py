#!/usr/bin/python3
# -*- coding: utf-8 -*-

from xml.sax import make_parser
from urllib.request import urlretrieve
from smil import SMILHandler
import sys


def fichero(etiquetas):
    n = 0  # Iniciamos la varibale en 0
    for etiqueta in etiquetas:
        for att, url in etiqueta['attrs']:
            if att == 'src':
                nombre_arch = url.split('/')[-1]
                ext = nombre_arch.split('.')[-1]
                urlretrieve(url, f'fichero-{n}.{ext}')
                n += 1


def main():
    # Programa principal
    nombre = sys.argv[1]

    parser = make_parser()  # analizador
    cHandler = SMILHandler()  # objeto handler de la clase ChistesHandler
    parser.setContentHandler(cHandler)
    parser.parse(open(nombre))  # empieza a analizar el fichero karaoke.smil(name)
    fichero(cHandler.get_tags())


if __name__ == '__main__':
    main()
