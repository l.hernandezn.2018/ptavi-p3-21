#!/usr/bin/python3
# -*- coding: utf-8 -*-

from xml.sax import make_parser
from xml.sax.handler import ContentHandler


class SMILHandler(ContentHandler):
    """"Clase que reconocera las etiquetas"""

    def __init__(self):
        """Inicializamos una lista que recogera las etiquetas y las etiquetas con sus atributos"""
        self.list = []
        # Creamos las etiquetas que debe reconocer con sus atributos
        self.attrs = {'root-layout': ['width', 'height', 'background-color'],
                      'region': ['id', 'top', 'bottom', 'left', 'right'],
                      'img': ['src', 'region', 'begin', 'end', 'dur'],
                      'audio': ['src', 'begin', 'dur'],
                      'textstream': ['src', 'region', 'fill']}

    def startElement(self, name, attrs):
        """Método que se llama cuando se abre ua etiqueta"""
        newlist = []  # Creamos nueva lista para almacenar las otras listas
        if name in self.attrs:
            for att in self.attrs[name]:
                valor = attrs.get(att, '')
                if valor:
                    newlist.append((att, valor))  # Almacena los atri-
            self.list.append({'attrs': newlist, 'name': name})  # butos en el diccionario

    def get_tags(self):
        """Método que devuelve la lista"""
        return self.list


def main():
    """Programa principal"""
    parser = make_parser()  # analizador
    cHandler = SMILHandler()  # objeto handler de la clase ChistesHandler
    parser.setContentHandler(cHandler)
    parser.parse(open("karaoke.smil"))  # empieza a analizar el fichero karaoke.smil
    print(cHandler.get_tags())


if __name__ == '__main__':
    main()
