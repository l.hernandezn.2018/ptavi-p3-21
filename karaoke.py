#!/usr/bin/python3
# -*- coding: utf-8 -*-

import json
from smil import SMILHandler
from xml.sax import make_parser
import sys


def to_string(etiquetas):
    s = ''  # Inicializamos la variable vacía
    for etiqueta in etiquetas:
        s += etiqueta['name']  # Va añadiendo en la varible el nombre de la etiqueta
        for attr, valor in etiqueta['attrs']:
            s += f'\t{attr}="{valor}"'  # Añade la etiqueta atributo con el contenido de este(valor) con cadena f-string
        s += '\n'  # Añade el salto de linea
    return s


# Ejercicio6: (funcion to_json)
def to_json(etiquetas):
    todo = []  # Inicializamos la lista "todo", vacia
    for etiqueta in etiquetas:
        contenido = {k: v for k, v in etiqueta['attrs']}  # Creamos un diccionario con el contenido de los atributos
        todo.append({'name': etiqueta['name'], 'attrs': contenido})  # Se va añadiendo a la lista "todo".
    return json.dumps(todo)  # dumps serializa el objeto a string JSON


def parse_arg():
    try:
        sys.argv[1]
    except IndexError:
        sys.exit("Usage: python3 karaoke.py <file>")  # Si no pasamos un fichero saldrá mostrando el mensaje

    if '--json' in sys.argv:
        return True, sys.argv[1] if sys.argv[1] != '--json' else sys.argv[2]
    else:
        return False, sys.argv[1]


def main():
    # Método del programa principal
    es_json, nombre = parse_arg()

    parser = make_parser()  # analizador
    cHandler = SMILHandler()  # objeto handler de la clase ChistesHandler
    parser.setContentHandler(cHandler)
    parser.parse(open(nombre))  # empieza a analizar el fichero karaoke.smil(name)

    if es_json:
        print(to_json(cHandler.get_tags()))
    else:
        print(to_string(cHandler.get_tags()), end='')


if __name__ == '__main__':
    main()
